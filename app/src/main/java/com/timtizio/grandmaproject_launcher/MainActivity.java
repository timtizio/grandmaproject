package com.timtizio.grandmaproject_launcher;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebSettings;
import android.widget.Button;
import android.widget.Toast;

import java.util.Date;
import java.util.TimerTask;

import com.timtizio.grandmaproject_launcher.JSInterface;

public class MainActivity extends AppCompatActivity {

    private static final String URL_INDEX_LANDSCAPE = "file:///android_asset/www/index.html";
    private static final String URL_INDEX_PORTRAIT = "file:///android_asset/www/index_portrait.html";

    private static final int ORIENTATION_LANDSCAPE = 100;
    private static final int ORIENTATION_PORTRAIT = 101;

    private int maintCounter = 0;
    private CountDownTimer resetMaintCounterTask = null;

    private int currentOrientation = ORIENTATION_LANDSCAPE;

    private WebView mWebView;

    @Override
    public void onNewIntent(Intent intent) {
        int order = intent.getIntExtra("order", -1);
        Log.d("TIMDBG", "onReceive type = " + order);

        if(order == JSInterface.ORDER_CHANGE_URL) {
            // Change url
            mWebView.loadUrl(intent.getStringExtra("url"));
        } else {
            loadIndex();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initWebview();

        Button maintBtn = findViewById(R.id.maint_btn);

        maintBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageMaintBtnClick();
            }
        });

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        int orientation = newConfig.orientation;
        if (currentOrientation != orientation) {
            loadIndex();
        }
    }
    private void initWebview() {
        mWebView = (WebView) findViewById(R.id.webview);

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                //Required functionality here
                return super.onJsAlert(view, url, message, result);
            }
        });

        mWebView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });

        JSInterface mJSInterface = new JSInterface(getApplicationContext(), MainActivity.class);
        mWebView.addJavascriptInterface(mJSInterface, "JavascriptInterface");

        WebSettings webViewSettings = mWebView.getSettings();
        webViewSettings.setJavaScriptEnabled(true);
        webViewSettings.setAllowFileAccess(true);

        loadIndex();
    }

    private void manageMaintBtnClick() {
        maintCounter++;

        if(resetMaintCounterTask != null) {
            resetMaintCounterTask.cancel();
        }

        if(maintCounter > 6) {
            resetPreferredLauncherAndOpenChooser(MainActivity.this);
        }

        resetMaintCounterTask = new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.d("TIMDBG", "onTick (maintCounter = " + maintCounter + ")");
            }

            @Override
            public void onFinish() {
                maintCounter = 0;
                resetMaintCounterTask = null;
                Log.d("TIMDBG", "onFinish (maintCounter = " + maintCounter + ")");
            }
        };

        resetMaintCounterTask.start();
    }

    public static void resetPreferredLauncherAndOpenChooser(Context context) {
        PackageManager packageManager = context.getPackageManager();
        ComponentName componentName = new ComponentName(context, FakeLauncherActivity.class);
        packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

        Intent selector = new Intent(Intent.ACTION_MAIN);
        selector.addCategory(Intent.CATEGORY_HOME);
        selector.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(selector);

        packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);
    }

    private void loadIndex() {
        int orientation = getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mWebView.loadUrl(URL_INDEX_LANDSCAPE);
        } else if(orientation == Configuration.ORIENTATION_PORTRAIT) {
            mWebView.loadUrl(URL_INDEX_PORTRAIT);
        }
    }
}
