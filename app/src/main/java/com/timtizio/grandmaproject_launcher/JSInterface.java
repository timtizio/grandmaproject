package com.timtizio.grandmaproject_launcher;

import android.content.Context;
import android.content.Intent;
import android.webkit.JavascriptInterface;

import java.lang.Class;

public class JSInterface {

    private Context mContext;
    private Class mainActivityClass;

    public final static int ORDER_CHANGE_URL = 1;
    public static final String ON_DATA_RECEIVED_FROM_JS = "com.timtizio.grandmaproject_launcher.FROM_JS";

    public JSInterface(Context context, Class className) {
        mContext = context;
        mainActivityClass = className;
    }

    @JavascriptInterface
    public void launchPackage(String pkg) {
        Intent launchIntent = mContext.getPackageManager().getLaunchIntentForPackage(pkg);
        mContext.startActivity(launchIntent);
    }

    @JavascriptInterface
    public void changeUrl(String url) {
        Intent intent = new Intent(mContext, mainActivityClass);
        // intent.setAction(ON_DATA_RECEIVED_FROM_JS);
        intent.putExtra("order", ORDER_CHANGE_URL);
        intent.putExtra("url", url);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }
}
