function initOnClickItems() {
    var itemsElem = document.getElementsByClassName("item");

    for(var i = 0; i < itemsElem.length; i++) {
        itemsElem[i].addEventListener("click", onClickOnItem);
    }
}

function onClickOnItem(e) {
    var itemSelected;
    if(e.target.id.match(/^item-[0-9]+$/)) {
        itemSelected = e.target.id;
    } else {
        itemSelected = e.target.parentNode.id;
    }

    var ID = itemSelected.match(/[0-9]+$/)[0];
    console.log("Click sur l'item " + ID);

    processClick(ID);
}

function processClick(idItemSelected) {
    var item = g_items[idItemSelected];

    if(item.type === "url") {
        changeUrl(item.target);
    } else if(item.type === "apk") {
        launchApk(item.target);
    }
}

function launchApk(package) {
    if(typeof JavascriptInterface != "undefined") {
        JavascriptInterface.launchPackage(package);
    } else {
        // For debug
        console.log("Lancement de l'APK " + package);
    }
}

function changeUrl(url) {
    if(typeof JavascriptInterface != "undefined") {
        JavascriptInterface.changeUrl(url);
    } else {
        // For debug
        url = url.match(/\/[a-z]+\.html$/)[0];
        url = url.substring(1);
        window.location = url;
    }
}

initOnClickItems();
