const g_items = [
    {
        "id": 0,
        "name": "Google",
        "description": "Internet",
        "type": "apk",
        "target": "com.google.android.googlequicksearchbox"
    },
    {
        "id": 1,
        "name": "Youtube",
        "description": "Vidéos",
        "type": "apk",
        "target": "com.google.android.youtube"
    },
    {
        "id": 2,
        "name": "WhatsApp",
        "description": "Messagerie",
        "type": "apk",
        "target": "com.whatsapp"
    },
    {
        "id": 3,
        "name": "Gmail",
        "description": "Boite aux lettres",
        "type": "apk",
        "target": "com.google.android.gm"
    },
    {
        "id": 4,
        "name": "Bible",
        "description": "Lire la bible",
        "type": "apk",
        "target": "la.bible.francaise.gratuit"
    },
    {
        "id": 5,
        "name": "grands-reportages",
        "description": "Reportage",
        "type": "apk",
        "target": "com.niveales.grandsreportages"
    },
    {
        "id": 6,
        "name": "Appareil Photo",
        "description": "Appareil Photo",
        "type": "apk",
        "target": "com.sec.android.app.camera"
    },
    {
        "id": 7,
        "name": "Le bon coin",
        "description": "Le bon coin",
        "type": "apk",
        "target": "fr.leboncoin"
    },
    {
        "id": 8,
        "name": "Arte",
        "description": "Arte",
        "type": "apk",
        "target": "tv.arte.plus7"
    },
    {
        "id": 9,
        "name": "Journaux",
        "description": "Journaux",
        "type": "url",
        "target": "file:///android_asset/www/journaux.html"
    },
    {
        "id": 10,
        "name": "Météo",
        "description": "Météo",
        "type": "apk",
        "target": "fr.meteo"
    }
];
